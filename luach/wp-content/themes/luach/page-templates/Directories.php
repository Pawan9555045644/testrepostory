<?php
/**
 * Template Name: Directories
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<!DOCTYPE html> 
<html class="no-js">


<body>

<!-- Preloader -->
	<div id="preloader">
	    <div id="status"></div>
	</div>




<!--Revolution Slider begin-->
<section class="container-slider">	
	<div id="slider">	
		<ul>
			<?php	$args = array( 'post_type' => 'slider');
					$loop = new WP_Query( $args ); 
					while ( $loop->have_posts() ) : $loop->the_post();
echo'<li data-transition="fade" data-slotamount="7">';
				echo the_content();
		echo'</li>';
				endwhile;
				?>
		

		
		</ul>	      
	</div><!-- end Slider -->   
</section><!--end Revolution Slider-->









<!-- end separator -->
<section class="content section-bg-4" id="job">

<div class="section-title align-center">
			<h2>Directory</h2>
			<h3 class="sub-heading">Lorem ipsum dolor sit amet dabipus</h3>
		</div>

<div class="container-center">
	<div class="dirctoreis">
			
				<!--<h1><i>28th, November, 2014</i></h1>--><?php
    $args = array(
      'post_type' => 'job'
      
	  
    );
    $jobs = new WP_Query( $args );
    if( $jobs->have_posts() ) {
      while( $jobs->have_posts() ) {
        $jobs->the_post();
		
        ?>
		<div class="list">
     <h1><i><?php the_time('Y/m/d \a\t g:i A') ?></i><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h1>
            <?php //the_content() ?>
			 <p><?php //the_excerpt(); ?></p>
			 </div>
			<?php	
                 }			 
                }			
				else {
                echo 'Oh no jobs!';
				}
  ?>
			</div><!--list end here-->
			
	
	</div><!--dirctoreis end here-->
	



			
 
		 



</div>




</section>

</body>
<script type="text/javascript" >
$(document).ready(function() {
$(".big-button").click(function() {
    $('html, body').animate({
        scrollTop: $(".expose").offset().top
    }, 2000);
});
$(".service").click(function() {
    $('html, body').animate({
        scrollTop: $("#serv").offset().top
    }, 2000);
});

$(".about").click(function() {
    $('html, body').animate({
        scrollTop: $(".expose").offset().top
    }, 2000);
}); 
$(".jobs").click(function() {
    $('html, body').animate({
        scrollTop: $("#job").offset().top
    }, 2000);
});
$(".cdfooter").click(function() {
    $('html, body').animate({
        scrollTop: $("#footer").offset().top
    }, 2000);
});

});
</script> 
</html>

<?php get_footer(); ?>