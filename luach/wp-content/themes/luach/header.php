<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<title>Luach Hatzibur</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta content="True" name="HandheldFriendly" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="viewport" content="width=device-width" />

<!--*************************
*							*
*         CSS FILES			*
*							*
************************* -->

<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800&amp;amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>

<!-- Due to IE8 inabillity to load multiple font weights from Google Fonts, we need to import them separately -->
<!--[if lte IE 8]>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300" /> 
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:700" /> 
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:800" />
<![endif]-->

<!-- Font Awesome -->
<link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet">

<!--imports the main css file-->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/skins/blue.css" />
<!-- used for animation effects on elements -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/animate.min.css" />
<!-- styling for Nivo Lightbox gallery -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/nivo-lightbox.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css" />
<!-- styles for the Revolution Slider -->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/plugins/rs-plugin/css/settings.css" media="screen" />
<!--imports the media queries css file-->
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css" />


<!--*************************
*							*
*      JAVASCRIPT FILES	 	*
*							*
************************* -->

<!-- imports modernizr plugin for detecting browser features -->
<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.js"></script>

<!--[if IE 8]>
	<link href="css/ie8.css" rel="stylesheet" />
	<script src="js/respond.js"></script>	
<![endif]-->

</head>

<body <?php body_class(); ?>>
	<div id="page" class="hfeed site">
		<header id="header">
	<div class="container-center cf">
		<div class="logo">
			<a href="?page_id=4"><img src="<?php echo get_template_directory_uri(); ?>/images/logo/logo.png" alt="Logo Estimation" /></a>			
		</div><!-- end logo -->
		
		
		<!--<div class="without_scroll">-->
			<nav class="navigation">
				<a class="toggleMenu" href="#">Menu</a>
				
					<ul class="nav">
						<li><a href="?page_id=4">Home<span>Amazing Luach Hatzibur </span></a>
							
						</li>			
						<li><a class= "service" href="javascript:void(0)">Services<span>What do we do?</span></a>
							
						</li>
						<li><a class= "about" href="javascript:void(0)">About Us<span>Who we are? </span></a>
							
						</li>
						<li><a href="javascript:void(0)">Archive<span>Our Publication</span></a>
							
						</li> 
							
						</li>
						<li><a class= "cdfooter" href="javascript:void(0)">Contact<span>How to reach us?</span></a></li>
						<li><a class= "jobs" href="?page_id=159">Directory<span>Different Directories</span></a></li>
					</ul>
			</nav>
		<!--</div>-->	
	</div><!-- end container-center -->
	
<!--*************************
*							*
*      JAVASCRIPT FILES	 	*
*							*
************************* -->



<!--responisve attachment start-->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
<!--responisve attachment start-->

<!--imports jquery-->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-migrate-1.1.0.min.js"></script>	

<!-- used for the contact form -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.form.js"></script>
<!-- used for the the fun facts counter -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.countTo.js"></script>
<!-- for displaying flickr images -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jflickrfeed.min.js"></script>
<!-- used to trigger the animations on elements -->
<script src="<?php echo get_template_directory_uri(); ?>/js/waypoints.min.js"></script>
<!-- used to stick the navigation menu to top of the screen on smaller displays -->
<script src="<?php echo get_template_directory_uri(); ?>/js/waypoints-sticky.min.js"></script>
<!-- used for rotating through tweets -->
<script src="<?php echo get_template_directory_uri(); ?>/js/vTicker.js"></script>
<!-- imports jQuery UI, used for toggles, accordions, tabs and tooltips -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui-1.10.3.custom.min.js"></script>
<!-- used for smooth scrolling on local links -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.scrollTo-1.4.3.1-min.js"></script>
<!-- used for opening images in a Lightbox gallery -->
<script src="<?php echo get_template_directory_uri(); ?>/js/nivo-lightbox.min.js"></script>
<!-- used for displaying tweets -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.tweet.js"></script>
<!-- flexslider plugin, used for image galleries (blog post preview, portfolio single page) and carousels -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider-min.js"></script>
<!-- used for sorting portfolio items -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.isotope.js"></script>
<!-- for detecting Retina displays and loading images accordingly -->
<script src="<?php echo get_template_directory_uri(); ?>/js/retina-1.1.0.min.js"></script>
<!-- for dropdown menus -->
<script src="<?php echo get_template_directory_uri(); ?>/js/superfish.js"></script>
<!-- used for sharing post pages -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.sharrre.min.js"></script>
<!-- easing plugin for easing animation effects -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-easing-1.3.js"></script>
<!-- Slider Revolution plugin -->
<script src="<?php echo get_template_directory_uri(); ?>/plugins/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>			
<script src="<?php echo get_template_directory_uri(); ?>/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>			

<!--imports custom javascript code-->
<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
<!--preview oonly option panel code -->
<script src="<?php echo get_template_directory_uri(); ?>/js/options.js"></script>







</header>

		<div id="main" class="site-main">
